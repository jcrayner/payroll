## Assumptions

- The only set of tax brackets provided are applicable to all inputs
- The input date range must be a valid monthly range (1st to end of month)
- The input date range year shall always be the current year
- An invalid input (invalid record or not matching available tax brackets) will result in skipping processing for the input rather than bailing out of the application

## Requirements
- Java 8

## Testing
The application has two sets of test suites.
- A set of unit tests
- A set of integration tests which run the application end to end


    ./gradlew test

or

    ./gradlew integrationTests

## Building
The application is built using gradle. This will manufacture an uber jar, so all required libraries are not required to be added to the classpath when invoking java.

    ./gradlew shadowJar

## Running
An example of running the application, feeding in a test file via stdin and redirecting stderr to /dev/null (application logger writes to stderr). Output is written to stdout by default. A user of the application can redirect stdout to other locations as they please using standard linux commands.

    java -jar build/libs/payroll-1.0-SNAPSHOT-all.jar < src/test/integration/resources/fixtures/test-input.csv  2> /dev/null

