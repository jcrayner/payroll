package com.myob.payroll.time.format;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;

/**
 * Common date-time formatters
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
public final class Formatters {
  private static final DateTimeFormatter FORMATTER = new DateTimeFormatterBuilder()
      .appendDayOfMonth(2)
      .appendLiteral(" ")
      .appendMonthOfYearText()
      .toFormatter()
      .withDefaultYear(LocalDate.now().getYear());

  private Formatters() { }

  public static DateTimeFormatter standardFormatter() {
    return FORMATTER;
  }
}
