package com.myob.payroll.module;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.datatype.guava.GuavaModule;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.afterburner.AfterburnerModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.google.common.base.Throwables;
import com.google.common.io.Resources;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.myob.payroll.calculator.DefaultPayrollCalculator;
import com.myob.payroll.calculator.PayrollCalculator;
import com.myob.payroll.model.InputRecord;
import com.myob.payroll.model.SalaryPackage;
import com.myob.payroll.model.TaxRule;
import java.io.IOException;
import java.util.Set;
import javax.validation.Validation;
import javax.validation.Validator;
import org.hibernate.validator.HibernateValidator;
import org.hibernate.validator.HibernateValidatorConfiguration;
import org.modelmapper.ModelMapper;

/**
 * Guice injection module for payroll application
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
public class ApplicationModule extends AbstractModule {
  @Provides
  public Set<TaxRule> taxRuleSet(ObjectMapper mapper) {
    try {
      return mapper.readValue(Resources.getResource("rules/tax-rules.json"), new TypeReference<Set<TaxRule>>() {});
    } catch(IOException e) {
      Throwable cause = Throwables.getRootCause(e);
      Throwables.throwIfUnchecked(cause);
      throw new RuntimeException(cause);
    }
  }

  @Provides
  public ObjectMapper objectMapper() {
    ObjectMapper objectMapper = new ObjectMapper();
    configureMapper(objectMapper);

    return objectMapper;
  }

  @Provides
  public CsvMapper csvMapper() {
    CsvMapper csvMapper = new CsvMapper();
    configureMapper(csvMapper);

    return csvMapper;
  }

  @Provides
  public ModelMapper modelMapper() {
    ModelMapper modelMapper = new ModelMapper();
    modelMapper.createTypeMap(InputRecord.class, SalaryPackage.class)
        .setProvider(
            request -> {
              InputRecord source = InputRecord.class.cast(request.getSource());
              return new SalaryPackage(source.getAnnualSalary(), source.getSuperRate());
            }
        );

    return modelMapper;
  }


  @Override
  protected void configure() {
    bind(PayrollCalculator.class).to(DefaultPayrollCalculator.class);
  }

  private void configureMapper(ObjectMapper mapper) {
    mapper.registerModule(new GuavaModule());
    mapper.registerModule(new AfterburnerModule());
    mapper.registerModule(new ParameterNamesModule());
    mapper.registerModules(new Jdk8Module());
    mapper.registerModules(new JavaTimeModule());
  }
}
