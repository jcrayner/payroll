package com.myob.payroll.calculator;

import com.google.common.collect.Range;
import com.google.inject.Inject;
import com.myob.payroll.model.SalaryPackage;
import com.myob.payroll.model.PaymentRecord;
import com.myob.payroll.model.TaxRule;
import com.myob.payroll.model.TaxRule.TaxPayableRule;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Set;

/**
 * Default payroll calculator. Using an injected set of tax rules, process an incoming salary package record into an
 * appropriate payment record.
 *
 *
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
public class DefaultPayrollCalculator implements PayrollCalculator {
  private static final RoundingMode ROUNDING_MODE = RoundingMode.HALF_UP;
  private static final Integer NO_OF_MONTHS = 12;

  private final Set<TaxRule> rules;

  @Inject
  public DefaultPayrollCalculator(Set<TaxRule> rules) {
    this.rules = rules;
  }

  public PaymentRecord calculate(final SalaryPackage record) throws NoTaxBracketFoundException {
    final Integer annualSalary = record.getAnnualSalary();
    final TaxRule matchingRule = rules.stream().filter((rule) -> rule.getBracket().contains(annualSalary))
        .findFirst().orElseThrow(() -> new NoTaxBracketFoundException(annualSalary, rules));

    final Range<Integer> bracket = matchingRule.getBracket();
    final Integer grossIncome = new BigDecimal(annualSalary / NO_OF_MONTHS).setScale(0, ROUNDING_MODE).intValue();
    final Integer taxPayable = new BigDecimal(matchingRule.getTaxable().map((payableRule) -> calculateTaxPayable(payableRule, bracket, annualSalary) / NO_OF_MONTHS).orElse(0D))
        .setScale(0, ROUNDING_MODE)
        .intValue();

    final Integer netIncome = grossIncome - taxPayable;
    final Integer superPayment = new BigDecimal(grossIncome * record.getSuperRate()).setScale(0, ROUNDING_MODE).intValue();
    return new PaymentRecord(grossIncome, taxPayable, netIncome, superPayment);
  }

  private Double calculateTaxPayable(TaxPayableRule taxRule, Range<Integer> taxBracket, Integer annualSalary) {
    return taxRule.getFixedAmount().orElse(0) +
        (annualSalary - Math.max(taxBracket.lowerEndpoint() - 1, 0)) *
        (taxRule.getCentsPerDollar() / 100);
  }
}
