package com.myob.payroll.calculator;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Range;
import com.myob.payroll.model.TaxRule;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.text.StrSubstitutor;

/**
 * An exception to be thrown when no appropriate tax rule can be found for an income
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
public class NoTaxBracketFoundException extends Exception {
  public NoTaxBracketFoundException(Integer income, Set<TaxRule> rules) {
    super(
        StrSubstitutor.replace(
            "No matching tax bracket found for an income of ${income}. Valid tax brackets are ${brackets}",
            ImmutableMap.of(
                "income", income,
                "brackets", rules.stream().map(TaxRule::getBracket)
                    .map(Range::toString)
                    .collect(Collectors.joining(", "))
            )
        )
    );
  }
}
