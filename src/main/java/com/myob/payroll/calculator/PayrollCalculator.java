package com.myob.payroll.calculator;

import com.myob.payroll.model.PaymentRecord;
import com.myob.payroll.model.SalaryPackage;

/**
 * Interface used to describe a payroll calculator
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
public interface PayrollCalculator {
  PaymentRecord calculate(final SalaryPackage record) throws NoTaxBracketFoundException;
}
