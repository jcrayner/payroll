package com.myob.payroll.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.MoreObjects;
import com.myob.payroll.serde.IntervalDeserializer;
import com.myob.payroll.serde.PercentageDeserializer;
import com.myob.payroll.validation.Extended;
import com.myob.payroll.validation.Month;
import java.util.Objects;
import javax.validation.GroupSequence;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import org.joda.time.Interval;

/**
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
@GroupSequence({InputRecord.class, Extended.class})
@JsonPropertyOrder(value = {"firstName", "lastName", "annualSalary", "superRate", "paymentInterval"})
public class InputRecord {
  @NotNull
  private final String firstName;
  @NotNull
  private final String lastName;
  @NotNull
  @PositiveOrZero
  private final Integer annualSalary;
  @NotNull
  @PositiveOrZero
  private final Double superRate;
  @NotNull
  @Month(groups = Extended.class)
  private final Interval paymentInterval;

  public InputRecord(
      @JsonProperty("firstName") final String firstName,
      @JsonProperty("lastName") final String lastName,
      @JsonProperty("annualSalary") final Integer annualSalary,
      @JsonProperty("superRate") @JsonDeserialize(using = PercentageDeserializer.class) final Double superRate,
      @JsonProperty("paymentInterval") @JsonDeserialize(using = IntervalDeserializer.class) final Interval paymentInterval
  ) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.annualSalary = annualSalary;
    this.superRate = superRate;
    this.paymentInterval = paymentInterval;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public Integer getAnnualSalary() {
    return annualSalary;
  }

  public Double getSuperRate() {
    return superRate;
  }

  public Interval getPaymentInterval() {
    return paymentInterval;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    InputRecord that = (InputRecord) o;
    return Objects.equals(firstName, that.firstName) &&
        Objects.equals(lastName, that.lastName) &&
        Objects.equals(annualSalary, that.annualSalary) &&
        Objects.equals(superRate, that.superRate) &&
        Objects.equals(paymentInterval, that.paymentInterval);
  }

  @Override
  public int hashCode() {
    return Objects.hash(firstName, lastName, annualSalary, superRate, paymentInterval);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("firstName", firstName)
        .add("lastName", lastName)
        .add("annualSalary", annualSalary)
        .add("superRate", superRate)
        .add("paymentInterval", paymentInterval)
        .toString();
  }
}
