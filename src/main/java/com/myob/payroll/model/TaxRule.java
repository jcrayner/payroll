package com.myob.payroll.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;
import com.google.common.collect.Range;
import java.util.Objects;
import java.util.Optional;
import javax.annotation.Nullable;

/**
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
public class TaxRule {
  private final Range<Integer> bracket;
  private final TaxPayableRule taxable;

  @JsonCreator
  public TaxRule(
      @JsonProperty("bracket") final Range<Integer> bracket,
      @JsonProperty("taxable") @Nullable final TaxPayableRule taxable
  ) {
    this.bracket = bracket;
    this.taxable = taxable;
  }

  public Range<Integer> getBracket() {
    return bracket;
  }

  public Optional<TaxPayableRule> getTaxable() {
    return Optional.ofNullable(taxable);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TaxRule taxRule = (TaxRule) o;
    return Objects.equals(bracket, taxRule.bracket) &&
        Objects.equals(taxable, taxRule.taxable);
  }

  @Override
  public int hashCode() {
    return Objects.hash(bracket, taxable);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("bracket", bracket)
        .add("taxable", taxable)
        .toString();
  }

  public static class TaxPayableRule {
    private final Integer fixedAmount;
    private final Double centsPerDollar;

    @JsonCreator
    public TaxPayableRule(
        @JsonProperty("fixedAmount") @Nullable final Integer fixedAmount,
        @JsonProperty("centsPerDollar") final Double centsPerDollar
    ) {
      this.fixedAmount = fixedAmount;
      this.centsPerDollar = centsPerDollar;
    }

    public Optional<Integer> getFixedAmount() {
      return Optional.ofNullable(fixedAmount);
    }

    public Double getCentsPerDollar() {
      return centsPerDollar;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      TaxPayableRule that = (TaxPayableRule) o;
      return Objects.equals(fixedAmount, that.fixedAmount) &&
          Objects.equals(centsPerDollar, that.centsPerDollar);
    }

    @Override
    public int hashCode() {
      return Objects.hash(fixedAmount, centsPerDollar);
    }

    @Override
    public String toString() {
      return MoreObjects.toStringHelper(this)
          .add("fixedAmount", fixedAmount)
          .add("centsPerDollar", centsPerDollar)
          .toString();
    }
  }
}
