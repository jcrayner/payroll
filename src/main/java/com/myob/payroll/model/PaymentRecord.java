package com.myob.payroll.model;

import com.google.common.base.MoreObjects;
import java.util.Objects;

/**
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
public class PaymentRecord {
  private final Integer grossIncome;
  private final Integer incomeTax;
  private final Integer netIncome;
  private final Integer superPayment;

  public PaymentRecord(Integer grossIncome, Integer incomeTax, Integer netIncome, Integer superPayment) {
    this.grossIncome = grossIncome;
    this.incomeTax = incomeTax;
    this.netIncome = netIncome;
    this.superPayment = superPayment;
  }

  public Integer getGrossIncome() {
    return grossIncome;
  }

  public Integer getIncomeTax() {
    return incomeTax;
  }

  public Integer getNetIncome() {
    return netIncome;
  }

  public Integer getSuperPayment() {
    return superPayment;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PaymentRecord paymentRecord = (PaymentRecord) o;
    return Objects.equals(grossIncome, paymentRecord.grossIncome) &&
        Objects.equals(incomeTax, paymentRecord.incomeTax) &&
        Objects.equals(netIncome, paymentRecord.netIncome) &&
        Objects.equals(superPayment, paymentRecord.superPayment);
  }

  @Override
  public int hashCode() {
    return Objects.hash(grossIncome, incomeTax, netIncome, superPayment);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("grossIncome", grossIncome)
        .add("incomeTax", incomeTax)
        .add("netIncome", netIncome)
        .add("superPayment", superPayment)
        .toString();
  }
}
