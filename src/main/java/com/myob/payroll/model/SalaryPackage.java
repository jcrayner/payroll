package com.myob.payroll.model;

import com.google.common.base.MoreObjects;
import java.util.Objects;

/**
 * A simple internal model that represents the salary package of an employee
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
public class SalaryPackage {
  private final Integer annualSalary;
  private final Double superRate;

  public SalaryPackage(final Integer annualSalary, final Double superRate) {
    this.annualSalary = annualSalary;
    this.superRate = superRate;
  }

  public Integer getAnnualSalary() {
    return annualSalary;
  }

  public Double getSuperRate() {
    return superRate;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SalaryPackage that = (SalaryPackage) o;
    return Objects.equals(annualSalary, that.annualSalary) &&
        Objects.equals(superRate, that.superRate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(annualSalary, superRate);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("annualSalary", annualSalary)
        .add("superRate", superRate)
        .toString();
  }
}
