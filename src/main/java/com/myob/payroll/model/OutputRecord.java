package com.myob.payroll.model;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.base.MoreObjects;
import com.myob.payroll.serde.IntervalSerializer;
import java.util.Objects;
import org.joda.time.Interval;

/**
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
@JsonPropertyOrder(value = {"name", "period", "grossIncome", "incomeTax", "netIncome", "superPayment"})
public class OutputRecord {
  private final String name;
  private final Interval period;
  private final Integer grossIncome;
  private final Integer incomeTax;
  private final Integer netIncome;
  private final Integer superPayment;

  public OutputRecord(
      final String name,
      final Interval period,
      final Integer grossIncome,
      final Integer incomeTax,
      final Integer netIncome,
      final Integer superPayment
  ) {
    this.name = name;
    this.period = period;
    this.grossIncome = grossIncome;
    this.incomeTax = incomeTax;
    this.netIncome = netIncome;
    this.superPayment = superPayment;
  }

  public OutputRecord(InputRecord inputRecord, PaymentRecord paymentRecord) {
    this(
        inputRecord.getFirstName() + " " + inputRecord.getLastName(),
        inputRecord.getPaymentInterval(),
        paymentRecord.getGrossIncome(),
        paymentRecord.getIncomeTax(),
        paymentRecord.getNetIncome(),
        paymentRecord.getSuperPayment()
    );
  }

  public String getName() {
    return name;
  }

  @JsonSerialize(using = IntervalSerializer.class)
  public Interval getPeriod() {
    return period;
  }

  public Integer getGrossIncome() {
    return grossIncome;
  }

  public Integer getIncomeTax() {
    return incomeTax;
  }

  public Integer getNetIncome() {
    return netIncome;
  }

  public Integer getSuperPayment() {
    return superPayment;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OutputRecord that = (OutputRecord) o;
    return Objects.equals(name, that.name) &&
        Objects.equals(period, that.period) &&
        Objects.equals(grossIncome, that.grossIncome) &&
        Objects.equals(incomeTax, that.incomeTax) &&
        Objects.equals(netIncome, that.netIncome) &&
        Objects.equals(superPayment, that.superPayment);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, period, grossIncome, incomeTax, netIncome, superPayment);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("name", name)
        .add("period", period)
        .add("grossIncome", grossIncome)
        .add("incomeTax", incomeTax)
        .add("netIncome", netIncome)
        .add("superPayment", superPayment)
        .toString();
  }
}
