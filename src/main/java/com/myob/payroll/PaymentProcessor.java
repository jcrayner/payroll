package com.myob.payroll;

import com.google.inject.Inject;
import com.myob.payroll.calculator.NoTaxBracketFoundException;
import com.myob.payroll.calculator.PayrollCalculator;
import com.myob.payroll.model.InputRecord;
import com.myob.payroll.model.SalaryPackage;
import com.myob.payroll.model.PaymentRecord;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import javax.validation.Validator;
import org.modelmapper.ModelMapper;

/**
 * The payment processor validates the input before passing it through to the calculator for payment calculation
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
public class PaymentProcessor {
  private final ModelMapper modelMapper;
  private final PayrollCalculator calculator;

  @Inject
  PaymentProcessor(ModelMapper modelMapper, PayrollCalculator calculator) {
    this.modelMapper = modelMapper;
    this.calculator  = calculator;
  }

  public PaymentRecord process(@Valid InputRecord record) throws NoTaxBracketFoundException {
    return calculator.calculate(modelMapper.map(record, SalaryPackage.class));
  }
}
