package com.myob.payroll.serde;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdScalarSerializer;
import com.myob.payroll.time.format.Formatters;
import java.io.IOException;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.joda.time.Interval;

/**
 * Custom serializer object which will write out Interval objects in the established payroll start / end date format.
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
public class IntervalSerializer extends StdScalarSerializer<Interval> {
  public IntervalSerializer() {
    super(Interval.class);
  }

  @Override
  public void serialize(Interval value, JsonGenerator gen, SerializerProvider provider) throws IOException {
    gen.writeString(Stream.of(value.getStart(), value.getEnd()).map(Formatters.standardFormatter()::print).collect(Collectors.joining(" - ")));
  }
}
