package com.myob.payroll.serde;

import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.FromStringDeserializer;
import com.google.common.base.Splitter;
import com.myob.payroll.time.format.Formatters;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.LocalDate;

/**
 * Custom interval deserializer which will transform the established payroll start / end dates into an Interval
 * object.
 *
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
public class IntervalDeserializer extends FromStringDeserializer<Interval> {
  private static final Splitter SPLITTER = Splitter.on("-").trimResults();

  public IntervalDeserializer() {
    super(Interval.class);
  }

  @Override
  protected Interval _deserialize(String value, DeserializationContext ctxt) throws IOException {
    List<DateTime> parts = SPLITTER.splitToList(value).stream()
        .map((input) -> LocalDate.parse(input, Formatters.standardFormatter()).toDateTimeAtStartOfDay())
        .collect(Collectors.toList());

    if(parts.size() != 2) {
      return (Interval) ctxt.handleWeirdStringValue(handledType(), value, "Interval requires both a start and end date separated by a '-'");
    }

    return new Interval(parts.get(0), parts.get(1));
  }
}