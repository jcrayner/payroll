package com.myob.payroll.serde;

import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.FromStringDeserializer;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;

/**
 * Custom deserializer for transforming string percentage input into the appropriate representation as a Double
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
public class PercentageDeserializer extends FromStringDeserializer<Double> {
  private static final DecimalFormat FORMATTER = new DecimalFormat("0.0#%");

  public PercentageDeserializer() {
    super(Double.class);
  }

  @Override
  protected Double _deserialize(String value, DeserializationContext ctxt) throws IOException {
    try {
      return FORMATTER.parse(value).doubleValue();
    } catch(ParseException e) {
      return (Double) ctxt.handleWeirdStringValue(handledType(), value, e.getMessage());
    }
  }
}
