package com.myob.payroll.validation;

import com.myob.payroll.validation.Month.MonthIntervalValidator;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import org.joda.time.Interval;
import org.joda.time.LocalDate;

/**
 * Validation annotation for objects which should represent whole months
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
@Target({ElementType.FIELD, ElementType.CONSTRUCTOR, ElementType.METHOD, ElementType.PARAMETER, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = MonthIntervalValidator.class)
public @interface Month {
  String message() default "{com.myob.payroll.validation.Month.message}";
  Class<?>[] groups() default {};
  Class<? extends Payload>[] payload() default {};

  /**
   * Validation implementation for validating joda Interval objects. A valid interval is currently defined as follows:
   * A single month if the start date is the first, the end date is the last day of the month, both start and end are
   * the same month in the same year
   */
  class MonthIntervalValidator implements ConstraintValidator<Month, Interval> {
    @Override
    public void initialize(Month constraintAnnotation) {
      // NO-OP
    }

    @Override
    public boolean isValid(Interval value, ConstraintValidatorContext context) {
      final LocalDate startDate = value.getStart().toLocalDate();
      final LocalDate endDate   = value.getEnd().toLocalDate();

      return startDate.getDayOfMonth() == 1 &&
          endDate.getDayOfMonth() == endDate.dayOfMonth().getMaximumValue() &&
          startDate.getMonthOfYear() == endDate.getMonthOfYear() &&
          startDate.getYear() == endDate.getYear();
    }
  }
}
