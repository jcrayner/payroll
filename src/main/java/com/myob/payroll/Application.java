package com.myob.payroll;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.RuntimeJsonMappingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.csv.CsvGenerator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.myob.payroll.calculator.NoTaxBracketFoundException;
import com.myob.payroll.model.InputRecord;
import com.myob.payroll.model.OutputRecord;
import com.myob.payroll.model.PaymentRecord;
import com.myob.payroll.module.ApplicationModule;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import org.hibernate.validator.HibernateValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.vyarus.guice.validator.ImplicitValidationModule;

/**
 * Payroll processing application.
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
public class Application {
  private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

  private final CsvMapper csvMapper;
  private final PaymentProcessor paymentProcessor;

  @Inject
  Application(CsvMapper csvMapper, PaymentProcessor paymentProcessor) {
    this.csvMapper        = csvMapper;
    this.paymentProcessor = paymentProcessor;
  }

  public static void main(String[] args) {
    Injector injector = Guice.createInjector(
        new ApplicationModule(),
        new ImplicitValidationModule(Validation.byProvider(HibernateValidator.class).configure().buildValidatorFactory())
    );

    Application application = injector.getInstance(Application.class);
    Runtime.getRuntime().addShutdownHook(new Thread(() -> LOGGER.info("Shutting down application")));
    
    try(final InputStream input = new BufferedInputStream(System.in)) {
      application.run(input, System.out);
    } catch(Throwable e) {
      LOGGER.error("An unexpected error has occurred whilst processing the payroll input", e);
      System.exit(1);
    }
  }

  public void run(InputStream streamIn, OutputStream streamOut) throws IOException, NoTaxBracketFoundException {
    CsvSchema inputSchema = csvMapper.typedSchemaFor(InputRecord.class).withoutHeader();
    MappingIterator<InputRecord> recordIn = csvMapper.readerFor(InputRecord.class)
        .with(inputSchema.withColumnSeparator(CsvSchema.DEFAULT_COLUMN_SEPARATOR))
        .readValues(streamIn);

    CsvGenerator generator = csvMapper.getFactory().createGenerator(streamOut);
    ObjectWriter writer    = csvMapper.writerWithSchemaFor(OutputRecord.class).with(SerializationFeature.FLUSH_AFTER_WRITE_VALUE);

    while (recordIn.hasNext()) {
      InputRecord inputRecord = null;

      try {
        inputRecord = recordIn.next();
        final PaymentRecord payable = paymentProcessor.process(inputRecord);
        writer.writeValue(generator, new OutputRecord(inputRecord, payable));
      } catch(ConstraintViolationException e) {
        LOGGER.warn("An invalid record was provided, skipping", e);
      } catch(NoTaxBracketFoundException e) {
        LOGGER.warn("No tax bracket found for input record {}, skipping", inputRecord, e);
      } catch(RuntimeJsonMappingException e) {
        LOGGER.warn(e.getMessage(), e);
      }
    }
  }
}
