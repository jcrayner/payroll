package com.myob.payroll.time.format;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import org.joda.time.LocalDate;
import org.junit.Test;

/**
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
public class FormattersTest {
  @Test
  public void testParsingOfDatesWorksAsExpected() throws Exception {
    assertThat(
        Formatters.standardFormatter().parseLocalDate("31 March"),
        is(equalTo(new LocalDate(LocalDate.now().getYear(), 3, 31)))
    );
  }

  @Test
  public void testWritingOfDatesIsTheExpectedFormat() throws Exception {
    assertThat(
        Formatters.standardFormatter().print(new LocalDate(LocalDate.now().getYear(), 3, 31)),
        is(equalTo("31 March"))
    );
  }
}