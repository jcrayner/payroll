package com.myob.payroll.calculator;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Range;
import com.myob.payroll.model.SalaryPackage;
import com.myob.payroll.model.TaxRule;
import com.myob.payroll.model.TaxRule.TaxPayableRule;
import java.util.Collections;
import org.hamcrest.Matchers;
import org.hamcrest.beans.HasPropertyWithValue;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
public class DefaultPayrollCalculatorTest {
  @Rule
  public ExpectedException thrown = ExpectedException.none();

  @Test
  public void testThatASalaryWithNoApplicableTaxBracketResultsInNoTaxBracketFoundException() throws Exception {
    thrown.expect(NoTaxBracketFoundException.class);
    DefaultPayrollCalculator calculator = new DefaultPayrollCalculator(Collections.emptySet());
    calculator.calculate(new SalaryPackage(120000, 0.09));
  }

  @Test
  public void testThatASalaryOfZeroDoesNotResultInAnException() throws Exception {
    DefaultPayrollCalculator calculator = new DefaultPayrollCalculator(ImmutableSet.of(new TaxRule(Range.atLeast(0), null)));
    assertThat(
        calculator.calculate(new SalaryPackage(0, 0D)),
        Matchers.allOf(
            HasPropertyWithValue.hasProperty("grossIncome", is(equalTo(0))),
            HasPropertyWithValue.hasProperty("incomeTax", is(equalTo(0))),
            HasPropertyWithValue.hasProperty("netIncome", is(equalTo(0))),
            HasPropertyWithValue.hasProperty("superPayment", is(equalTo(0)))
        )
    );
  }

  @Test
  public void testCalculationOfPaymentWithNoTaxPayable() throws Exception {
    DefaultPayrollCalculator calculator = new DefaultPayrollCalculator(ImmutableSet.of(new TaxRule(Range.closed(0, 18200), null)));

    // validate the outcome of calculation
    assertThat(
        calculator.calculate(new SalaryPackage(10000, 0.1)),
        Matchers.allOf(
            HasPropertyWithValue.hasProperty("grossIncome", is(equalTo(833))),
            HasPropertyWithValue.hasProperty("incomeTax", is(equalTo(0))),
            HasPropertyWithValue.hasProperty("netIncome", is(equalTo(833))),
            HasPropertyWithValue.hasProperty("superPayment", is(equalTo(83)))
        )
    );
  }

  @Test
  public void testCalculationOfPaymentWithPercentageOnly() throws Exception {
    DefaultPayrollCalculator calculator = new DefaultPayrollCalculator(
        ImmutableSet.of(
            new TaxRule(
                Range.closed(18201, 37000),
                new TaxPayableRule(null, 19D)
            )
        )
    );

    // validate the outcome of calculation
    assertThat(
        calculator.calculate(new SalaryPackage(30000, 0.1)),
        Matchers.allOf(
            HasPropertyWithValue.hasProperty("grossIncome", is(equalTo(2500))),
            HasPropertyWithValue.hasProperty("incomeTax", is(equalTo(187))),
            HasPropertyWithValue.hasProperty("netIncome", is(equalTo(2313))),
            HasPropertyWithValue.hasProperty("superPayment", is(equalTo(250)))
        )
    );
  }

  @Test
  public void testCalculationOfPaymentWithFixedAmountAndPercentage() throws Exception {
    DefaultPayrollCalculator calculator = new DefaultPayrollCalculator(
        ImmutableSet.of(
            new TaxRule(
                Range.closed(37001, 87000),
                new TaxPayableRule(3572, 32.5)
            )
        )
    );

    // validate the outcome of calculation
    assertThat(
        calculator.calculate(new SalaryPackage(60000, 0.09)),
        Matchers.allOf(
            HasPropertyWithValue.hasProperty("grossIncome", is(equalTo(5000))),
            HasPropertyWithValue.hasProperty("incomeTax", is(equalTo(921))),
            HasPropertyWithValue.hasProperty("netIncome", is(equalTo(4079))),
            HasPropertyWithValue.hasProperty("superPayment", is(equalTo(450)))
        )
    );
  }
}