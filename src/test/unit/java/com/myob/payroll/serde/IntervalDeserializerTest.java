package com.myob.payroll.serde;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.myob.payroll.time.format.Formatters;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
public class IntervalDeserializerTest {
  static ObjectMapper MAPPER = new ObjectMapper();

  @Rule
  public ExpectedException thrown = ExpectedException.none();

  @BeforeClass
  public static void setUp() {
    SimpleModule module = new SimpleModule();
    module.addDeserializer(Interval.class, new IntervalDeserializer());
    MAPPER.registerModule(module);
  }

  @Test
  public void testDeserialize() throws Exception {
    assertThat(
        MAPPER.readValue("\"01 March - 31 March\"", Interval.class),
        is(
            equalTo(
              new Interval(
                  DateTime.parse("01 March", Formatters.standardFormatter()).withTimeAtStartOfDay(),
                  DateTime.parse("31 March", Formatters.standardFormatter()).withTimeAtStartOfDay()
              )
            )
        )
    );
  }

  @Test
  public void testDeserializeWithMissingEndDate() throws Exception {
    thrown.expect(JsonMappingException.class);
    MAPPER.readValue("\"01 March\"", Interval.class);
  }
}