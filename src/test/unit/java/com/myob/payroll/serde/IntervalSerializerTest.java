package com.myob.payroll.serde;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.myob.payroll.time.format.Formatters;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
public class IntervalSerializerTest {
  static ObjectMapper MAPPER = new ObjectMapper();

  @BeforeClass
  public static void setUp() {
    SimpleModule module = new SimpleModule();
    module.addSerializer(Interval.class, new IntervalSerializer());
    MAPPER.registerModule(module);
  }

  @Test
  public void testSerialize() throws Exception {
    assertThat(
        MAPPER.writeValueAsString(
            new Interval(
                DateTime.parse("01 March", Formatters.standardFormatter()).withTimeAtStartOfDay(),
                DateTime.parse("31 March", Formatters.standardFormatter()).withTimeAtStartOfDay()
            )
        ),
        is(equalTo("\"01 March - 31 March\""))
    );
  }
}