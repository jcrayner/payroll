package com.myob.payroll.serde;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
public class PercentageDeserializerTest {
  static ObjectMapper MAPPER = new ObjectMapper();

  @Rule
  public ExpectedException thrown = ExpectedException.none();

  @BeforeClass
  public static void setUp() {
    SimpleModule module = new SimpleModule();
    module.addDeserializer(Double.class, new PercentageDeserializer());
    MAPPER.registerModule(module);
  }

  @Test
  public void testDeserialize() throws Exception {
    assertThat(MAPPER.readValue("\"20%\"", Double.class), is(equalTo(0.2D)));
  }

  @Test
  public void testDeserializeWithMissingPercentageSymbol() throws Exception {
    thrown.expect(JsonMappingException.class);
    MAPPER.readValue("\"20\"", Double.class);
  }
}