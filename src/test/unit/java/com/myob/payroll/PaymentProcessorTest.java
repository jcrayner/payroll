package com.myob.payroll;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.myob.payroll.model.InputRecord;
import com.myob.payroll.module.ApplicationModule;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;

import org.hibernate.validator.HibernateValidator;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.vyarus.guice.validator.ImplicitValidationModule;

/**
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
public class PaymentProcessorTest {
  private static PaymentProcessor PROCESSOR;

  @Rule
  public ExpectedException thrown = ExpectedException.none();

  @BeforeClass
  public static void beforeClass() {
    Injector injector = Guice.createInjector(
            new ApplicationModule(),
            new ImplicitValidationModule(Validation.byProvider(HibernateValidator.class).configure().buildValidatorFactory())
    );

    PROCESSOR = injector.getInstance(PaymentProcessor.class);
  }

  @Test
  public void testThatAnInvalidPaymentIntervalResultsInAConstraintViolation() throws Exception {
    thrown.expect(ConstraintViolationException.class);
    PROCESSOR.process(
        new InputRecord(
            "John",
            "Doe",
            12000,
            0.09,
            new Interval(DateTime.parse("2017-03-02"), DateTime.parse("2017-03-31"))
        )
    );
  }

  @Test
  public void testThatNullFieldsResultsInAConstraintViolation() throws Exception {
    thrown.expect(ConstraintViolationException.class);
    PROCESSOR.process(new InputRecord(null, null, null, null, null));
  }

  @Test
  public void testThatNegativeNumbersResultsInAConstraintViolation() throws Exception {
    thrown.expect(ConstraintViolationException.class);
    PROCESSOR.process(
        new InputRecord(
            "John",
            "Doe",
            -12000,
            -0.09,
            new Interval(DateTime.parse("2017-03-02"), DateTime.parse("2017-03-31"))
        )
    );
  }
}