package com.myob.payroll;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalToIgnoringWhiteSpace;
import static org.hamcrest.core.Is.is;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import com.myob.payroll.categories.IntegrationTest;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import org.junit.Test;
import org.junit.experimental.categories.Category;

/**
 * Basic end to end test of standard and negative test case scenarios. Ideally, you wouldn't compare two strings like
 * this as it is somewhat brittle. Not suitable for large datasets as everything is compared in memory and it places
 * importance on the ordering of the output when there seems to be no business rule which expects an ordered output (eg FIFO).
 * However, this does the job for now.
 *
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
@Category(IntegrationTest.class)
public class ApplicationTests {
  @Test
  public void testEnd2End() throws Exception {
    final String expected    = Resources.toString(Resources.getResource("fixtures/test-output.csv"), Charsets.UTF_8);

    // maintain OG STDIN/STDOUT
    final InputStream stdin  = System.in;
    final PrintStream stdout = System.out;

    try(final InputStream input = Resources.getResource("fixtures/test-input.csv").openStream()) {
      System.setIn(input);

      try(final OutputStream stream = new ByteArrayOutputStream()) {
        final PrintStream output = new PrintStream(stream, true, Charsets.UTF_8.displayName());
        System.setOut(output);

        Application.main(null);
        assertThat(stream.toString(), is(equalToIgnoringWhiteSpace(expected)));
      } finally {
        System.setOut(stdout); // reset STDOUT
      }
    } finally {
      System.setIn(stdin); // reset STDIN
    }
  }
}
