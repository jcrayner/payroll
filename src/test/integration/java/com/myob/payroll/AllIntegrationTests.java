package com.myob.payroll;

import com.myob.payroll.categories.IntegrationTest;
import org.junit.experimental.categories.Categories;
import org.junit.experimental.categories.Categories.IncludeCategory;
import org.junit.runner.RunWith;
import org.junit.runners.Suite.SuiteClasses;

/**
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
@RunWith(Categories.class)
@IncludeCategory(IntegrationTest.class)
@SuiteClasses({ApplicationTests.class})
public class AllIntegrationTests {

}
